################################################################################
# Define a keras layer class that allows for permanent pruning,
# a pruning function, and a plotting function
################################################################################

# imports  copied from keras.layers.core
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import copy
import sys
import textwrap
import types as python_types
import warnings

import numpy as np

from tensorflow.python.eager import backprop
from tensorflow.python.eager import context
from tensorflow.python.framework import constant_op
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.framework import tensor_shape
from tensorflow.python.keras import activations
from tensorflow.python.keras import backend as K
from tensorflow.python.keras import constraints
from tensorflow.python.keras import initializers
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.engine.base_layer import Layer
from tensorflow.python.keras.engine.input_spec import InputSpec
from tensorflow.python.keras.utils import conv_utils
from tensorflow.python.keras.utils import generic_utils
from tensorflow.python.keras.utils import tf_utils
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import gen_math_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import nn
from tensorflow.python.ops import sparse_ops
from tensorflow.python.ops import standard_ops
from tensorflow.python.ops import variable_scope
from tensorflow.python.platform import tf_logging
from tensorflow.python.training.tracking import base as trackable
from tensorflow.python.util import nest
from tensorflow.python.util import tf_inspect
from tensorflow.python.util.tf_export import keras_export

# NEW: some more imports
import tensorflow as tf
from tensorflow.keras.layers import *
import networkx as nx
import matplotlib
from matplotlib import pyplot as plt

################################################################################

# The following class is a copy of Dense in keras. I marked all the lines that 
# I added or changed

class DenseWithMask(Layer):
  """Dense layer but with optional permanent masking of units or edges."""

  def __init__(self, 
               units,
               unit_mask=None, # NEW
               edge_mask=None, # NEW
               activation=None,
               use_bias=True,
               kernel_initializer='glorot_uniform',
               bias_initializer='zeros',
               kernel_regularizer=None,
               bias_regularizer=None,
               activity_regularizer=None,
               kernel_constraint=None,
               bias_constraint=None,
               **kwargs):
    if 'input_shape' not in kwargs and 'input_dim' in kwargs:
      kwargs['input_shape'] = (kwargs.pop('input_dim'),)

    super(DenseWithMask, self).__init__( # changed 'Dense' to 'DenseWithMask'
        activity_regularizer=regularizers.get(activity_regularizer), **kwargs)

    self.units = int(units) if not isinstance(units, int) else units 
    # NEW: add unit_mask to class attributes
    self.unit_mask = unit_mask 
    # NEW: add edge_mask to class attributes
    self.edge_mask = edge_mask 
    # NEW: add unit_mask_indices to class attributes
    self.unit_mask_indices = None 
    # NEW: add edge_mask_indices to class attributes
    self.edge_mask_indices = None 
    self.activation = activations.get(activation)
    self.use_bias = use_bias
    self.kernel_initializer = initializers.get(kernel_initializer)
    self.bias_initializer = initializers.get(bias_initializer)
    self.kernel_regularizer = regularizers.get(kernel_regularizer)
    self.bias_regularizer = regularizers.get(bias_regularizer)
    self.kernel_constraint = constraints.get(kernel_constraint)
    self.bias_constraint = constraints.get(bias_constraint)

    self.supports_masking = True
    self.input_spec = InputSpec(min_ndim=2)

    
  def build(self, input_shape): 
    dtype = dtypes.as_dtype(self.dtype or K.floatx())
    if not (dtype.is_floating or dtype.is_complex):
      raise TypeError('Unable to build `Dense` layer with non-floating point '
                      'dtype %s' % (dtype,))
    input_shape = tensor_shape.TensorShape(input_shape)
    if tensor_shape.dimension_value(input_shape[-1]) is None:
      raise ValueError('The last dimension of the inputs to `Dense` '
                       'should be defined. Found `None`.')
    last_dim = tensor_shape.dimension_value(input_shape[-1])
    self.input_spec = InputSpec(min_ndim=2, axes={-1: last_dim})
    
    # NEW: update masks in build
    if self.unit_mask is None:
        self.unit_mask = np.ones(self.units, dtype=bool)
    else:
        # check if previously set mask matches number of units
        if not len(self.unit_mask) == self.units:
            raise ValueError('Length of unit_mask must be equal to number of units.')
            
    if self.edge_mask is None:
        self.edge_mask = np.ones((last_dim, self.units), dtype=bool)
    else:
        # check if previously set mask matches input dimensions
        if not self.edge_mask.shape == (last_dim, self.units):
            raise ValueError('Dimensions of edge_mask must be equal to (last_input_dim, units).')
                      
    # NEW: incorporate unit_mask info into edge_mask
    self.edge_mask = self.edge_mask * self.unit_mask
    
    # NEW: incorporate edge_mask info into unit_mask
    self.unit_mask = self.unit_mask * np.any(self.edge_mask, axis=0)
    
    # NEW: update mask indices
    self.edge_mask_indices = np.stack(self.edge_mask.nonzero()).T
    self.edge_mask_indices = np.array(self.edge_mask_indices, dtype='int64')
    self.unit_mask_indices = self.unit_mask.nonzero()[0]
    # need to convert indices for bias to 2d array for sparse tensor
    self.unit_mask_indices = np.stack([np.zeros(len(self.unit_mask_indices)),
                                       self.unit_mask_indices]).T
    self.unit_mask_indices = np.array(self.unit_mask_indices, dtype='int64')
    
    # NEW: turn all new attributes into tensorflow constants
    self.unit_mask = tf.constant(self.unit_mask)
    self.edge_mask = tf.constant(self.edge_mask)
    self.unit_mask_indices = tf.constant(self.unit_mask_indices)
    self.edge_mask_indices = tf.constant(self.edge_mask_indices)
    
    self.kernel = self.add_weight(
        'kernel',
        # NEW: trainable kernel weights may be fewer than before;
        # they are now stored as 1D array,
        shape=[int(np.sum(self.edge_mask))], 
        initializer=self.kernel_initializer,
        regularizer=self.kernel_regularizer,
        constraint=self.kernel_constraint,
        dtype=self.dtype,
        trainable=True)
    if self.use_bias:
      self.bias = self.add_weight(
          'bias',
          #NEW: trainable biases may be fewer than number of units
          shape=[int(np.sum(self.unit_mask))], 
          initializer=self.bias_initializer,
          regularizer=self.bias_regularizer,
          constraint=self.bias_constraint,
          dtype=self.dtype,
          trainable=True)
    else:
      self.bias = None
    self.built = True
    
    
  def rebuild(self, edge_mask=None, unit_mask=None): 
    # NEW: This is a new function for rebuilding a DenseWithMask layer with a 
    # new edge mask and/or unit mask
    
    # if none are given, get default values for masks from layer
    if edge_mask is None:
        edge_mask = self.edge_mask.numpy()
    if unit_mask is None:
        unit_mask = self.unit_mask.numpy()
        
    # incorporate unit_mask info into edge_mask
    edge_mask = edge_mask * unit_mask
    
    # incorporate edge_mask info into unit_mask
    unit_mask = unit_mask * np.any(edge_mask, axis=0)
    
    # NOW: get new arrays of trainable weights for layer
    # The stuff below contains slow, redundant lines but 
    # those might become handy when adding default values for
    # new edges and nodes(?)
    
    # create old kernel
    kernel_old = np.zeros_like(self.edge_mask, dtype=float)
    kernel_old[self.edge_mask.numpy()]=self.kernel.numpy()
    
    # create new kernel
    kernel_new = np.zeros_like(edge_mask, dtype=float)
    for i in range(len(kernel_new)):
        for j in range(len(kernel_new[0])):
            if edge_mask[i,j]:
                kernel_new[i,j] = kernel_old[i,j]
                
    # create initializer for new kernel
    vals = list(kernel_new[edge_mask])
    initK = tf.compat.v1.keras.initializers.Constant(value=vals, 
                                                     verify_shape=False)
                
    # save new edge mask
    self.edge_mask = edge_mask
    
    # build new kernel
    self.kernel = self.add_weight(
        'kernel',
        shape=[int(np.sum(self.edge_mask))], 
        initializer=initK,
        regularizer=self.kernel_regularizer,
        constraint=self.kernel_constraint,
        dtype=self.dtype,
        trainable=True)
    
    # create old bias list
    bias_old = np.zeros_like(self.unit_mask, dtype=float)
    bias_old[self.unit_mask.numpy()] = self.bias.numpy()
    
    # create new bias list
    bias_new = np.zeros_like(unit_mask, dtype=float)
    for i in range(len(bias_new)):
        if unit_mask[i]:
            bias_new[i] = bias_old[i]
    
    # create initializer for new biases
    vals = list(bias_new[unit_mask])
    initB = tf.compat.v1.keras.initializers.Constant(value=vals, 
                                                     verify_shape=False)
    
    # save new unit mask
    self.unit_mask = unit_mask
    
    # build new biases
    self.bias = self.add_weight(
        'bias',
        shape=[int(np.sum(self.unit_mask))], 
        initializer=initB,
        regularizer=self.bias_regularizer,
        constraint=self.bias_constraint,
        dtype=self.dtype,
        trainable=True)
    
    # update mask indices
    self.edge_mask_indices = np.stack(self.edge_mask.nonzero()).T
    self.edge_mask_indices = np.array(self.edge_mask_indices, dtype='int64')
    self.unit_mask_indices = self.unit_mask.nonzero()[0]
    # need to convert indices for bias to 2d array for sparse tensor
    self.unit_mask_indices = np.stack([np.zeros(len(self.unit_mask_indices)),
                                       self.unit_mask_indices]).T
    self.unit_mask_indices = np.array(self.unit_mask_indices, dtype='int64')
    
    # turn all new attributes into tensorflow constants
    self.unit_mask = tf.constant(self.unit_mask)
    self.edge_mask = tf.constant(self.edge_mask)
    self.unit_mask_indices = tf.constant(self.unit_mask_indices)
    self.edge_mask_indices = tf.constant(self.edge_mask_indices)
    
    
  def call(self, inputs): 
    
    # NEW: create a kernel (2D numpy array) from 1D list of trainable kernel weights
    kernel = tf.SparseTensor(indices=self.edge_mask_indices, 
                             values=self.kernel, 
                             dense_shape=self.edge_mask.shape)
    kernel = tf.sparse.to_dense(kernel)
    
    # NEW: create a bias vector (1D numpy array) from 1D list of trainable biases
    bias = tf.SparseTensor(indices=self.unit_mask_indices, values=self.bias, 
                           dense_shape=[1,self.unit_mask.shape[0]])
    bias = tf.squeeze(tf.sparse.to_dense(bias))

    rank = inputs.shape.rank
    if rank is not None and rank > 2:
      # Broadcasting is required for the inputs.
      outputs = standard_ops.tensordot(inputs, kernel, [[rank - 1], [0]]) # NEW: self.kernel -> kernel
      # Reshape the output back to the original ndim of the input.
      if not context.executing_eagerly():
        shape = inputs.shape.as_list()
        output_shape = shape[:-1] + [self.units]
        outputs.set_shape(output_shape)
    else:
      inputs = math_ops.cast(inputs, self._compute_dtype)
      if K.is_sparse(inputs):
        outputs = sparse_ops.sparse_tensor_dense_matmul(inputs, kernel) # NEW: self.kernel -> kernel
      else:
        outputs = gen_math_ops.mat_mul(inputs, kernel) # NEW: self.kernel -> kernel
        
    if self.use_bias:
      outputs = nn.bias_add(outputs, bias) # NEW: self.bias -> bias
    
    if self.activation is not None:
      return self.activation(outputs)  

    return outputs


  def compute_output_shape(self, input_shape):
    input_shape = tensor_shape.TensorShape(input_shape)
    input_shape = input_shape.with_rank_at_least(2)
    if tensor_shape.dimension_value(input_shape[-1]) is None:
      raise ValueError(
          'The innermost dimension of input_shape must be defined, but saw: %s'
          % input_shape)
    
    return input_shape[:-1].concatenate(self.units)


  def get_config(self): #done?
    config = {
        'units': self.units,
        'unit_mask': self.unit_mask, #NEW: added unit_mask to config
        'edge_mask': self.edge_mask, #NEW: added edge_mask to config
        'activation': activations.serialize(self.activation), 
        'use_bias': self.use_bias,
        'kernel_initializer': initializers.serialize(self.kernel_initializer), 
        'bias_initializer': initializers.serialize(self.bias_initializer),
        'kernel_regularizer': regularizers.serialize(self.kernel_regularizer), 
        'bias_regularizer': regularizers.serialize(self.bias_regularizer),
        'activity_regularizer':
            regularizers.serialize(self.activity_regularizer),
        'kernel_constraint': constraints.serialize(self.kernel_constraint), 
        'bias_constraint': constraints.serialize(self.bias_constraint)
    }
    base_config = super(Dense, self).get_config() 
    
    return dict(list(base_config.items()) + list(config.items()))


################################################################################

# Define functions for Sequential class: prune and draw_network

def get_pruning_scores(self, function=None, bias_function=None):
    # helper function for prune
    
    if function is None:
        function = lambda ew, uw: np.abs(ew)
        
    if bias_function is None:
        bias_function = lambda ew, uw: np.abs(uw)

    fvals_k = []
    fvals_b = []
    ems = []
    ums = []
    
    for i, layer in enumerate(self.layers):
        if isinstance(layer, DenseWithMask):
                    
            # get masks
            em = layer.edge_mask.numpy()
            um = layer.unit_mask.numpy()
                   
            if em is None or um is None:
                raise ValueError('Model must be built before pruning.')
                        
            # get weights
            kernel_weights = layer.kernel.numpy()
            bias_weights = layer.bias.numpy()
                    
            # recreate kernel matrix and bias vector
            kernel = np.zeros_like(em, dtype=float)
            kernel[em] = kernel_weights
            bias = np.zeros_like(um, dtype=float)
            bias[um]=bias_weights
                    
            # compute prune function
            fvals = np.zeros_like(em, dtype=float)
            for j in range(len(kernel)):
                for k in range(len(kernel[0])):
                    fvals[j,k] = function(kernel[j,k],bias[k])
                    
            fvals2 = np.zeros_like(um, dtype=float)
            for j in range(len(bias)):
                fvals2[j] = bias_function(kernel[:,j],bias[j])
              
            fvals_k = fvals_k + [fvals]
            fvals_b = fvals_b + [fvals2]
            ems = ems + [em]
            ums = ums + [um]
            
    return [fvals_k, ems, fvals_b, ums]
            

def prune(self, ratio=None, threshold=None, bias_ratio=None, bias_threshold=None,
          function=None, bias_function=None, # what is this?
          prune_random=False, prune_across_layers=True,
          prune_weights=True, prune_biases=False):
    
    if prune_random:
        # random pruning
        
        #print('prune random')
               
        for i, layer in enumerate(self.layers):
            if isinstance(layer, DenseWithMask):
                
                # get masks
                em = layer.edge_mask.numpy()
                um = layer.unit_mask.numpy()
                
                if em is None or um is None:
                    raise ValueError('Model must be built before pruning.')
                
                # prune weights and/or biases
                if prune_weights:
                    
                    # set pruning ratio if none given
                    if ratio is None: 
                        ratio = 0.25
                        
                    # prune edges
                    new_elements = np.random.choice(a=[False, True], size=(np.sum(em)), 
                                                    p=[ratio, 1.0-ratio])
                    # only change elements that were True before
                    em[em] = new_elements
                    
                if prune_biases:
                    
                    # set pruning ratio if none given
                    if bias_ratio is None:
                        bias_ratio = 0.25

                    # prune nodes
                    new_elements = np.random.choice(a=[False, True], size=(np.sum(um)), 
                                                    p=[bias_ratio, 1.0-bias_ratio])
                    # only change elements that were True before
                    um[um] = new_elements
                    
                layer.rebuild(edge_mask=em, unit_mask=um)
            else:
                #raise NotImplementedError('Pruning only implemented for DenseWithMask layers.')
                # update: don't raise error; skip layer
                continue
     
    else:
        # non-random pruning      
                           
        if prune_weights:
            
            # set pruning ratio if neither ratio or threshold given
            if ratio is None and threshold is None:
                ratio = 0.25
                
            # if no prune function given set function for edge pruning
            # to return edge weight 
            if function is None:
                function = lambda ew, uw: np.abs(ew)
                
        if prune_biases:
            
            # set pruning ratio if neither ratio or threshold given
            if bias_ratio is None and bias_threshold is None:
                bias_ratio = 0.25
                
            # if no function given set function for node pruning to return
            # the sum of absolute edge weights and the absolute node bias
            if bias_function is None:
                bias_function = lambda ew, uw: np.sum(np.abs(ew))+np.abs(uw)
                
        # compute values of function and bias_function
        fvals_k, ems, fvals_b, ums = self.get_pruning_scores(function=function, 
                                                             bias_function=bias_function)
                
        if prune_across_layers:
            
            if prune_weights and threshold is None:
                
                # make a single flat array of weights
                fvals = np.concatenate([np.ravel(fvals_k[i][ems[i]]) for i in range(len(ems))])
                
                # get threshold
                index = int(len(fvals)*ratio)
                sorted_fvals = np.sort(fvals)
                threshold = sorted_fvals[index]    
                print('threshold', threshold)
                
            if prune_biases and bias_threshold is None:
                
                # make a single flat array of biases
                fvals = np.concatenate([np.ravel(fvals_b[i][ums[i]])
                                       for i in range(len(ums))])   
                
                # get bias threshold
                index = int(len(fvals)*bias_ratio)
                sorted_fvals = np.sort(fvals)
                bias_threshold = sorted_fvals[index]    
                print('bias threshold', bias_threshold)
                
        # start pruning
        i0 = -1
        for i, layer in enumerate(self.layers):
            if isinstance(layer, DenseWithMask):
                
                i0 += 1
                
                if prune_weights:
                    # prune edges
                                       
                    if threshold is None:
                        
                        print('set threshold')
                        
                        # compute number of edges to be pruned
                        prune_num = int(np.round(np.sum(ems[i0])*ratio))
                        
                        # compute corresponding threshold
                        threshold = np.sort(fvals_k[i0][ems[i0]], axis=None)[prune_num]
                        
                    print('threshold', threshold)

                    for j in range(len(fvals_k[i0])):
                        for k in range(len(fvals_k[i0][0])):
                            if fvals_k[i0][j,k] < threshold:
                                ems[i0][j,k] = False
                                                        
                if prune_biases:
                    # prune nodes
                    
                    if bias_threshold is None:
                        
                        # compute number of edges to be pruned
                        prune_num = int(np.round(np.sum(ums[i0])*bias_ratio))
                        
                        # compute corresponding threshold
                        bias_threshold = np.sort(fvals_b[i0][ums[i0]], axis=None)[prune_num]
                            
                    #else: # use threshold for pruning
                    for j in range(len(fvals_b[i0])):
                        if fvals_b[i0][j] < bias_threshold:
                            ums[i0][j] = False
                
                layer.rebuild(edge_mask=ems[i0], unit_mask=ums[i0])
                
            else:
                #raise NotImplementedError('Pruning only implemented for DenseWithMask layers.')
                # update: don't raise error; skip layer
                continue
                
                
def draw_network(self, **kwds):
    
    # get number of layers
    num_layers = len(self.layers)
    
    # initialize arrays
    num_nodes = [0]*(num_layers+2)
    
    # loop over layers to collect nodes
    i = 0
    for layer in self.layers:
        
        if isinstance(layer, DenseWithMask):
            
            # get edge mask
            em = layer.edge_mask.numpy()
            
            if i==0:
                # first line of nodes represents features
                # get number of features in layer from edge_mask shape
                num_nodes[0] = em.shape[0]

            # get number of nodes in layer from edge_mask shape
            num_nodes[i+1] = em.shape[1]
            
            i += 1
            
    num_nodes = num_nodes[:i+1]
    
    # initialize adjacency matrix
    total_nodes = np.sum(num_nodes)
    A = np.zeros((total_nodes, total_nodes),dtype=float)
    
    # loop over layers to get adjacency-matrix elements
    i = 0
    for layer in self.layers:
        
        if i < len(num_nodes)-1:
    
            if isinstance(layer, DenseWithMask):
            
                # get kernel
                kernel = np.zeros_like(layer.edge_mask, dtype=float)
                kernel[layer.edge_mask.numpy()]=layer.kernel.numpy()
            
                # set adjacency-matrix elements with kernel weights
                n_prev = np.sum(num_nodes[:i]) # number of nodes in previous layers
                n_curr = np.sum(num_nodes[:i+1]) # number of nodes in previous and current layer
                A[int(n_prev):n_curr, n_curr:n_curr+num_nodes[i+1]]= kernel
                i += 1
    
    # make a network
    G = nx.from_numpy_matrix(A) 
    #G = nx.from_numpy_matrix(A, create_using=nx.DiGraph)
    # Use undirected network because it is faster!
    
    # make layout
    dx = 1.0/np.max([1,len(num_nodes)-1])
    pos_list = [(i*dx, 1.0-j/np.max([1,num_nodes[i]-1]))
                for i in range(len(num_nodes)) for j in range(num_nodes[i])]    
    pos_dict = {}
    for i, n in enumerate(G.nodes()):
        pos_dict[n] = pos_list[i]
    
    # draw network
    nx.draw(G, pos=pos_dict, **kwds) # todo: add colours for weighted edges     
    
    print('number of nodes:', G.number_of_nodes())
    print('number of edges:', G.number_of_edges())
                
        
# add to Sequential class
tf.keras.models.Sequential.get_pruning_scores = get_pruning_scores
tf.keras.models.Sequential.prune = prune
tf.keras.models.Sequential.draw_network = draw_network