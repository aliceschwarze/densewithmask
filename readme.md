# DenseWithMask

Keras layer that allows slow but permanent pruning of edges. Networks can be retrained after pruning
without pruned edges reappearing.

Folder contains the source code (.py file) and three Jupyter notebooks:

#### 1. `mnist_example.ipynb`
This notebook is just a very simple keras example using a sequential network of dense layers on the MNIST data set. (I got this from some keras tutorial but I can't find it anymore.)

#### 2. `DenseWithMask_example.ipynb`
This notebook is almost identical to the MNIST example code but it uses the `DenseWithMask` layer class instead of keras' `Dense`. The network is pruned by 20% before training.

#### 3. `simple_nn.ipynb`
This notebook provides code perform experiments where a network is trained, pruned, and then trained again for several cycles. (In the notebook, I call them "runs".) It includes several example networks (all Sequential models with DenseWithMask layers) of varying width and depth.

#### Comments on the source code

The only way that I found to implement permanent edge removal in keras was via recasting a 1-d array of trainable weights at each into a sparse tensor. This is insanely slow. Recommendations for a better approach would be very welcome!
